/*
 * @file GPIO.h
 * @author Derek Molloy
 * @version 0.1
 *
 * Created on: 29 Apr 2014
 * Copyright (c) 2014 Derek Molloy (www.derekmolloy.ie)
 * Made available for the book "Exploring BeagleBone"
 * See: www.exploringbeaglebone.com
 * Licensed under the EUPL V.1.1
 *
 * This Software is provided to You under the terms of the European
 * Union Public License (the "EUPL") version 1.1 as published by the
 * European Union. Any use of this Software, other than as authorized
 * under this License is strictly prohibited (to the extent such use
 * is covered by a right of the copyright holder of this Software).
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * For more details, see http://www.derekmolloy.ie/
 */

#ifndef GPIO_H_
#define GPIO_H_
#include <string>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

using std::string;
//using std::ofstream;
namespace fs = boost::filesystem;

namespace bbbdevlib {

const string GPIO_SYSPATH="/sys/class/gpio";
typedef int (*CallbackType)(int);

/**
 * @class GPIO
 * @brief GPIO class for input and output functionality on a single GPIO pin
 */
class GPIO {
public:
	enum DIRECTION{ INPUT, OUTPUT };
	enum VALUE{ LOW=0, HIGH=1 };
	enum EDGE{ NONE, RISING, FALLING, BOTH };
    enum GPIO_PIN{ P8_03=38, P8_04=39, P8_05=34, P8_06=35, P8_07=66, P8_08=67,
                   P8_09=69, P8_10=68, P8_11=45, P8_12=44, P8_13=23, P8_14=26,
                   P8_15=47, P8_16=46, P8_17=27, P8_18=65, P8_19=22, P8_20=63,
                   P8_21=62, P8_22=37, P8_23=36, P8_24=33, P8_25=32, P8_26=61,
                   P8_27=86, P8_28=88, P8_29=87, P8_30=89, P8_31=10, P8_32=11,
                   P8_33=9, P8_34=81, P8_35=8, P8_36=80, P8_37=78, P8_38=79,
                   P8_39=76, P8_40=77, P8_41=74, P8_42=75, P8_43=72, P8_44=73,
                   P8_45=70, P8_46=71, P9_11=30, P9_12=60, P9_13=31, P9_14=50,
                   P9_15=48, P9_16=51, P9_17=5, P9_18=4, P9_19=13, P9_20=12,
                   P9_21=3, P9_22=2, P9_23=49, P9_24=15, P9_25=117, P9_26=14,
                   P9_27=115, P9_28=113, P9_29=111, P9_30=112, P9_31=110,
                   P9_41A=20, P9_41B=116, P9_42A=7, P9_42B=114};

private:
	int pin;			/**< The GPIO number of the object */
	int debounceTime;   /**< The debounce time in milliseconds */
	string name;		/**< The name of the GPIO e.g. gpio50 */
	string path;  		/**< The full path to the GPIO e.g. /sys/class/gpio/gpio50/ */

public:
	GPIO(int pin);
	virtual int getNumber() { return this->pin; } /**< Returns the GPIO number as an int. */

	// General Input and Output Settings
	virtual int setDirection(GPIO::DIRECTION);
	virtual GPIO::DIRECTION getDirection();
	virtual int setValue(GPIO::VALUE);
	virtual int toggleOutput();
	virtual GPIO::VALUE getValue();
	virtual int setActiveLow(bool isLow=true);  //low=1, high=0
	virtual int setActiveHigh(); //default
	//software debounce input (ms) - default 0
	virtual void setDebounceTime(int time) { this->debounceTime = time; }

	// Advanced OUTPUT: Faster write by keeping the stream alive (~20X)
	virtual int streamOpen();
	virtual int streamWrite(GPIO::VALUE);
	virtual int streamClose();

	virtual int toggleOutput(int time); //threaded invert output every X ms.
	virtual int toggleOutput(int numberOfTimes, int time);
	virtual void changeToggleTime(int time) { this->togglePeriod = time; }
	virtual void toggleCancel() { this->threadRunning = false; }

	// Advanced INPUT: Detect input edges; threaded and non-threaded
	virtual int setEdgeType(GPIO::EDGE);
	virtual GPIO::EDGE getEdgeType();
	virtual int waitForEdge(); // waits until button is pressed
	virtual int waitForEdge(CallbackType callback); // threaded with callback
	virtual void waitForEdgeCancel() { this->threadRunning = false; }

	virtual ~GPIO();  //destructor will unexport the pin

private:
	int exportGPIO();
	int unexportGPIO();
	fs::ofstream stream;
	pthread_t thread;
	CallbackType callbackFunction;
	bool threadRunning;
	int togglePeriod;  //default 100ms
	int toggleNumber;  //default -1 (infinite)
	friend void* threadedPoll(void *value);
	friend void* threadedToggle(void *value);
};

void* threadedPoll(void *value);
void* threadedToggle(void *value);

} /* namespace bbbdevlib */

#endif /* GPIO_H_ */









