/*
 * PWM.cpp  Created on: 29 Apr 2014
 * Copyright (c) 2014 Derek Molloy (www.derekmolloy.ie)
 * Made available for the book "Exploring BeagleBone"
 * See: www.exploringbeaglebone.com
 * Licensed under the EUPL V.1.1
 *
 * This Software is provided to You under the terms of the European
 * Union Public License (the "EUPL") version 1.1 as published by the
 * European Union. Any use of this Software, other than as authorized
 * under this License is strictly prohibited (to the extent such use
 * is covered by a right of the copyright holder of this Software).
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * For more details, see http://www.derekmolloy.ie/
 */

#include "PWM.h"
#include "util.h"
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

namespace fs = boost::filesystem;
using namespace std;
namespace bbbdevlib {

PWM::PWM(int pinName) {
	this->name = pinName;
	this->path = "";
	this->pwmchip = "";
	this->analogFrequency = 100000;
	this->analogMax = 3.3;

    if (boost::filesystem::is_empty(PWM_SYSPATH)){
        cout << "Load a PWM Overlay first" << endl;
        exit (EXIT_FAILURE);
    } else {
        if (getPwmChip() == 0) {
            this->path = PWM_SYSPATH + "/" + this->pwmchip;
            if (exportPWM() < 0) {
                exit(EXIT_FAILURE);
            }
        }
    }
}

int PWM::getPwmChip(){
    int pwmchannel = -1;
    switch (this->name){
        case PWM::PWMPINS::P9_21:
        case PWM::PWMPINS::P9_22:
        case PWM::PWMPINS::P9_29:
        case PWM::PWMPINS::P9_31:
            pwmchannel = PWM::PWMMODULES::EHRPWM0;
            break;
        case PWM::PWMPINS::P9_14:
        case PWM::PWMPINS::P9_16:
        case PWM::PWMPINS::P8_34:
        case PWM::PWMPINS::P8_36:
            pwmchannel = PWM::PWMMODULES::EHRPWM1;
            break;
        case PWM::PWMPINS::P8_13:
        case PWM::PWMPINS::P8_19:
        case PWM::PWMPINS::P8_45:
        case PWM::PWMPINS::P8_46:
            pwmchannel = PWM::PWMMODULES::EHRPWM2;
            break;
        case PWM::PWMPINS::P9_42A:
            pwmchannel = PWM::PWMMODULES::ECAP0;
            break;
        case PWM::PWMPINS::P9_28:
            pwmchannel = PWM::PWMMODULES::ECAP2;
            break;
        default:
            cout << "GPIO " << name << ": Is not a valid PWM pin" << endl;
            return -1;
    }

    switch (pwmchannel){
        case PWM::PWMMODULES::EHRPWM0:
            this->pwmchip = match_pwmchip(EHRPWM0_PATH);
            break;
        case PWM::PWMMODULES::EHRPWM1:
            this->pwmchip = match_pwmchip(EHRPWM1_PATH);
            break;
        case PWM::PWMMODULES::EHRPWM2:
            this->pwmchip = match_pwmchip(EHRPWM2_PATH);
            break;
        case PWM::PWMMODULES::ECAP0:
            this->pwmchip = match_pwmchip(ECAP0_PATH);
            break;
        case PWM::PWMMODULES::ECAP2:
            this->pwmchip = match_pwmchip(ECAP2_PATH);
            break;
    }
    if (this->pwmchip == "Not Found") {
        cout << "Path to PWM module not found" << endl;
        return -1;
    }
    return 0;
}

string PWM::match_pwmchip(const string pwmmodulepath){
    vector< string > all_matching_files;
    const boost::regex my_filter("pwmchip.*");
    fs::directory_iterator end_iter; // Default ctor yields past-the-end
    const fs::path p(pwmmodulepath);
    if (fs::exists(p) && fs::is_directory(p))
    {
      for(fs::directory_iterator dir_iter(p) ; dir_iter != end_iter ; ++dir_iter)
      {
        if (!fs::is_directory(dir_iter->status())) continue;
        boost::smatch what;
        if(!boost::regex_match(dir_iter->path().filename().string(), what, my_filter)) continue;
        all_matching_files.push_back(what[0]);
        cout << all_matching_files[0] << endl;
        return all_matching_files[0];
      }
    }
    return "Not Found";
}

int PWM::exportPWM(){
    int pwmexport=-1;
    switch (this->name){
        case PWM::PWMPINS::P9_21:
        case PWM::PWMPINS::P9_29:
        case PWM::PWMPINS::P9_16:
        case PWM::PWMPINS::P8_34:
        case PWM::PWMPINS::P8_13:
        case PWM::PWMPINS::P8_19:
        case PWM::PWMPINS::P8_45:
        case PWM::PWMPINS::P8_46:
            pwmexport = 1;
            break;
        case PWM::PWMPINS::P9_22:
        case PWM::PWMPINS::P9_31:
        case PWM::PWMPINS::P9_14:
        case PWM::PWMPINS::P8_36:
        case PWM::PWMPINS::P9_42A:
        case PWM::PWMPINS::P9_28:
            pwmexport = 0;
            break;
        default:
            cout << "GPIO " << name << ": Is not a valid PWM pin" << endl;
    }
    if (pwmexport >= 0){
        string export_path = this->path + "/";

        if (pwmexport == 0) {
            fs::path pwmpath(this->path + "/pwm0");
            if (!fs::exists(pwmpath)){
                cout << "Exporting GPIO" << this->name << ": to " << export_path << EXPORT << endl;
                if (writefile(export_path, EXPORT, pwmexport) == 0){
                    usleep(250000); //delay while creating sysfs
                }
            }
            if (fs::exists(pwmpath) && fs::is_directory(pwmpath)) {
                this->path = pwmpath.string() + "/";
                return 0;
            }
        }
        else {
            fs::path pwmpath(this->path + "/pwm1");
            if (!fs::exists(pwmpath)){
                cout << "Exporting GPIO" << this->name << ": to " << export_path << EXPORT << endl;
                if (writefile(export_path, EXPORT, pwmexport) == 0){
                    usleep(250000); //delay while creating sysfs
                }
            }
            if (fs::exists(pwmpath) && fs::is_directory(pwmpath)) {
                this->path = pwmpath.string() + "/";
                return 0;
            }
        }
    }
    return -1;
}

int PWM::setPeriod(unsigned int period_ns){
	return writefile(this->path, PWM_PERIOD, period_ns);
}

unsigned int PWM::getPeriod(){
	return atoi(readfile(this->path, PWM_PERIOD).c_str());
}

float PWM::period_nsToFrequency(unsigned int period_ns){
	float period_s = (float)period_ns/1000000000;
	return 1.0f/period_s;
}

unsigned int PWM::frequencyToPeriod_ns(float frequency_hz){
	float period_s = 1.0f/frequency_hz;
	return (unsigned int)(period_s*1000000000);
}

int PWM::setFrequency(float frequency_hz){
	return this->setPeriod(this->frequencyToPeriod_ns(frequency_hz));
}

float PWM::getFrequency(){
	return this->period_nsToFrequency(this->getPeriod());
}

int PWM::setDutyCycle(unsigned int duty_ns){
	return writefile(this->path, PWM_DUTY, duty_ns);
}

int PWM::setDutyCycle(float percentage){
	if ((percentage>100.0f)||(percentage<0.0f)) return -1;
	unsigned int period_ns = this->getPeriod();
	float duty_ns = period_ns * (percentage/100.0f);
	this->setDutyCycle((unsigned int) duty_ns );
	return 0;
}

unsigned int PWM::getDutyCycle(){
	return atoi(readfile(this->path, PWM_DUTY).c_str());
}

float PWM::getDutyCyclePercent(){
	unsigned int period_ns = this->getPeriod();
	unsigned int duty_ns = this->getDutyCycle();
	return 100.0f * (float)duty_ns/(float)period_ns;
}

int PWM::setPolarity(PWM::POLARITY polarity){
    if (polarity == PWM::ACTIVE_HIGH){
        return writefile(this->path, PWM_POLARITY, "normal");
    }
    else {
        return writefile(this->path, PWM_POLARITY, "inversed");
    }
}

void PWM::invertPolarity(){
	if (this->getPolarity()==PWM::ACTIVE_LOW) {
        this->setPolarity(PWM::ACTIVE_HIGH);
	}
	else {
        this->setPolarity(PWM::ACTIVE_LOW);
	}
}

PWM::POLARITY PWM::getPolarity(){
	if (readfile(this->path, PWM_POLARITY)=="inversed") {
        return PWM::ACTIVE_LOW;
    }
	else {
	    return PWM::ACTIVE_HIGH;
    }
}

int PWM::calibrateAnalogMax(float analogMax){ //must be between 3.2 and 3.4
	if((analogMax<3.2f) || (analogMax>3.4f)) return -1;
	else this->analogMax = analogMax;
	return 0;
}

int PWM::analogWrite(float voltage){
	if ((voltage<0.0f)||(voltage>3.3f)) return -1;
	this->setFrequency(this->analogFrequency);
	this->setPolarity(PWM::ACTIVE_LOW);
	this->setDutyCycle((100.0f*voltage)/this->analogMax);
	return this->run();
}

int PWM::run(){
	return writefile(this->path, PWM_RUN, 1);
}

bool PWM::isRunning(){
	string running = readfile(this->path, PWM_RUN);
	return (running=="1");
}

int PWM::stop(){
	return writefile(this->path, PWM_RUN, 0);
}

PWM::~PWM() {}

} /* namespace bbbdevlib */
