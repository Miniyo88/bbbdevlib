/*
 * PWM.h  Created on: 29 Apr 2014
 * Copyright (c) 2014 Derek Molloy (www.derekmolloy.ie)
 * Made available for the book "Exploring BeagleBone"
 * See: www.exploringbeaglebone.com
 * Licensed under the EUPL V.1.1
 *
 * This Software is provided to You under the terms of the European
 * Union Public License (the "EUPL") version 1.1 as published by the
 * European Union. Any use of this Software, other than as authorized
 * under this License is strictly prohibited (to the extent such use
 * is covered by a right of the copyright holder of this Software).
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * For more details, see http://www.derekmolloy.ie/
 */

#ifndef PWM_H_
#define PWM_H_
#include <string>

using std::string;

namespace bbbdevlib {

const string EXPORT="export";
const string PWM_PERIOD="period";
const string PWM_DUTY="duty_cycle";
const string PWM_POLARITY="polarity";
const string PWM_RUN="enable";
const string OCP_PATH="/sys/devices/platform/ocp";
const string PWM_SYSPATH="/sys/class/pwm";
const string ECAP0_PATH="/sys/devices/platform/ocp/48300000.epwmss/48300100.ecap/pwm"; //to get pwmchipN
const string ECAP2_PATH="/sys/devices/platform/ocp/48304000.epwmss/48304100.ecap/pwm"; //to get pwmchipN
const string EHRPWM0_PATH="/sys/devices/platform/ocp/48300000.epwmss/48300200.ehrpwm/pwm"; //to get pwmchipN
const string EHRPWM1_PATH="/sys/devices/platform/ocp/48302000.epwmss/48302200.ehrpwm/pwm"; //to get pwmchipN
const string EHRPWM2_PATH="/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm"; //to get pwmchipN
/**
 * @class PWM
 * @brief A class to control a basic PWM output -- you must know the exact sysfs filename
 * for the PWM output.
 */
class PWM {
public:
	enum POLARITY{ ACTIVE_HIGH=0, ACTIVE_LOW=1 };
	enum PWMPINS { P8_13=23, P8_19=22, P8_34=81, P8_36=80, P8_45=70, P8_46=71, P9_14=50,
                   P9_16=51, P9_21=3, P9_22=2, P9_28=113, P9_29=111, P9_31=110, P9_42A=7, P9_42B=114 };
    enum PWMMODULES { EHRPWM0 = 0, EHRPWM1 = 1, EHRPWM2 = 2, ECAP0 = 3, ECAP2 = 4 };

private:
	string path, pwmchip;
	int name;
	float analogFrequency;  //defaults to 100,000 Hz
	float analogMax;        //defaults to 3.3V

public:
	PWM(int pinName);

	virtual int setPeriod(unsigned int period_ns);
	virtual unsigned int getPeriod();
	virtual int setFrequency(float frequency_hz);
	virtual float getFrequency();
	virtual int setDutyCycle(unsigned int duration_ns);
	virtual int setDutyCycle(float percentage);
	virtual unsigned int getDutyCycle();
	virtual float getDutyCyclePercent();

	virtual int setPolarity(PWM::POLARITY);
	virtual void invertPolarity();
	virtual PWM::POLARITY getPolarity();

	virtual void setAnalogFrequency(float frequency_hz) { this->analogFrequency = frequency_hz; }
	virtual int calibrateAnalogMax(float analogMax); //must be between 3.2 and 3.4
	virtual int analogWrite(float voltage);

	virtual int run();
	virtual bool isRunning();
	virtual int stop();

	virtual ~PWM();
private:
    string match_pwmchip(const string pwmmodulepath);
    int getPwmChip();
    int exportPWM();
	float period_nsToFrequency(unsigned int);
	unsigned int frequencyToPeriod_ns(float);
};

} /* namespace bbbdevlib */

#endif /* PWM_H_ */
